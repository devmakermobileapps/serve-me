import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { UiSwitchModule } from 'ngx-ui-switch';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PageContentComponent } from './page-content/page-content.component';
import { AppRoutingModule } from './app.routing-module';
import { DashboardComponent } from './page-content/dashboard/dashboard.component';
import { ConfiguracaoComponent } from './page-content/configuracao/configuracao.component';
import { GerenciarUsuarioComponent } from './page-content/dashboard/gerenciar-usuario/gerenciar-usuario.component';
import { NovoUsuarioComponent } from './page-content/dashboard/gerenciar-usuario/novo-usuario/novo-usuario.component';
import { CardInformacaoComponent } from './page-content/dashboard/card-informacao/card-informacao.component';
import { BreadcrumbComponent } from './header/breadcrumb/breadcrumb.component';
import { BreadcrumbService } from './header/breadcrumb/breadcrumb.service';
import { TitleComponent } from './header/title/title.component';
import { TitleService } from './header/title/title.service';
import { BotoesHeaderComponent } from './header/botoes-header/botoes-header.component';
import { BotoesHeaderService } from './header/botoes-header/botoes-header.service';
import { OrcamentosComponent } from './page-content/orcamentos/orcamentos.component';
import { ServicosComponent } from './page-content/servicos/servicos.component';
import { CategoriasServicosComponent } from './page-content/categorias-servicos/categorias-servicos.component';
import { DescontosComponent } from './page-content/descontos/descontos.component';
import { PagamentosComponent } from './page-content/pagamentos/pagamentos.component';
import { RelatoriosComponent } from './page-content/relatorios/relatorios.component';
import { FaleConoscoComponent } from './page-content/fale-conosco/fale-conosco.component';
import { AdminsComponent } from './page-content/admins/admins.component';
import { ClientesComponent } from './page-content/usuarios/clientes/clientes.component';
import { PrestadoresComponent } from './page-content/usuarios/prestadores/prestadores.component';
import { PushComponent } from './page-content/push/push.component';
import { NovoAdminComponent } from './page-content/admins/novo-admin/novo-admin.component';
import { NovoPushComponent } from './page-content/push/novo-push/novo-push.component';
import { NovoServicoComponent } from './page-content/categorias-servicos/servico/novo-servico/novo-servico.component';
import { DetalhesClienteComponent } from './page-content/usuarios/clientes/detalhes-cliente/detalhes-cliente.component';
import { DetalhesPrestadorComponent } from './page-content/usuarios/prestadores/detalhes-prestador/detalhes-prestador.component';
import { DetalhesOrcamentosComponent } from './page-content/orcamentos/detalhes-orcamentos/detalhes-orcamentos.component';
import { UserCredentialsComponent } from './header/user-credentials/user-credentials.component';
import { LoginComponent } from './login/login.component';
import { UserCredentialService } from './header/user-credentials/user-credential.service';
import { ServicoComponent } from './page-content/categorias-servicos/servico/servico.component';
import { NovoDescontoComponent } from './page-content/descontos/novo-desconto/novo-desconto.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    PageContentComponent,
	DashboardComponent,
	ConfiguracaoComponent,
	GerenciarUsuarioComponent,
	NovoUsuarioComponent,
	CardInformacaoComponent,
	BreadcrumbComponent,
	TitleComponent,
	BotoesHeaderComponent,
	OrcamentosComponent,
	ServicosComponent,
	CategoriasServicosComponent,
	DescontosComponent,
	PagamentosComponent,
	RelatoriosComponent,
	FaleConoscoComponent,
	AdminsComponent,
	ClientesComponent,
	PrestadoresComponent,
	DescontosComponent,
	PagamentosComponent,
	PushComponent,
	PushComponent,
	NovoAdminComponent,
	NovoPushComponent,
	NovoServicoComponent,
	DetalhesClienteComponent,
	DetalhesPrestadorComponent,
	DetalhesOrcamentosComponent,
	UserCredentialsComponent,
	LoginComponent,
	ServicoComponent,
	NovoDescontoComponent
],
imports: [
	BrowserModule,
	AppRoutingModule,
	MDBBootstrapModule.forRoot(),
	NgbModule.forRoot(),
	NgxDatatableModule,
	ChartsModule,
	ReactiveFormsModule,
	HttpModule,
	PerfectScrollbarModule,
	UiSwitchModule

  ],
  providers: [
	{
		provide: PERFECT_SCROLLBAR_CONFIG,
		useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
	},
	BreadcrumbService,
	TitleService,
	BotoesHeaderService,
	UserCredentialService
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
