import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../header/title/title.service';

@Component({
  selector: 'app-fale-conosco',
  templateUrl: './fale-conosco.component.html',
  styleUrls: ['./fale-conosco.component.scss']
})
export class FaleConoscoComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
		this.titleService.changeTitle('Fale conosco');
	  	this.breadcrumbService.changeRoute([], '');
	}

	ngOnInit() {
	}

}
