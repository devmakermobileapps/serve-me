import { Component, OnInit } from '@angular/core';
import { UserCredentialService } from '../../../../header/user-credentials/user-credential.service';

@Component({
  selector: 'app-detalhes-cliente',
  templateUrl: './detalhes-cliente.component.html',
  styleUrls: ['./detalhes-cliente.component.scss']
})
export class DetalhesClienteComponent implements OnInit {

	currentJustify = 'end'
	constructor(
		private userCredentialService:UserCredentialService
	) { 
		this.userCredentialService.toggleUser(true)
	}

	ngOnInit() {
	}

}
