import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../../header/title/title.service';
import { UserCredentialService } from '../../../header/user-credentials/user-credential.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService,
		
		
	) { 
	  	this.breadcrumbService.changeRoute([], '');
		this.titleService.changeTitle('Usuários Clientes');
	}

	ngOnInit() {
	}

}
