import { Component, OnInit } from '@angular/core';
import { UserCredentialService } from '../../../../header/user-credentials/user-credential.service';

@Component({
  selector: 'app-detalhes-prestador',
  templateUrl: './detalhes-prestador.component.html',
  styleUrls: ['./detalhes-prestador.component.scss']
})
export class DetalhesPrestadorComponent implements OnInit {
	
	currentJustify = 'end'

	constructor(
		private userCredentialService:UserCredentialService
	) { 
		this.userCredentialService.toggleUser(true)
	}

	ngOnInit() {
	}

}
