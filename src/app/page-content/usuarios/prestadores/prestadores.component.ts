import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../../header/title/title.service';

@Component({
  selector: 'app-prestadores',
  templateUrl: './prestadores.component.html',
  styleUrls: ['./prestadores.component.css']
})
export class PrestadoresComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
	  	this.breadcrumbService.changeRoute([], '');
		this.titleService.changeTitle('Usuários Prestadores');
	  
	}

	ngOnInit() {
	}

}
