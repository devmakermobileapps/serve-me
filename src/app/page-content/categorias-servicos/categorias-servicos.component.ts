import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../header/title/title.service';

@Component({
  selector: 'app-categorias-servicos',
  templateUrl: './categorias-servicos.component.html',
  styleUrls: ['./categorias-servicos.component.scss']
})
export class CategoriasServicosComponent implements OnInit {
	assistenciaTecnica:boolean;
	aulas:boolean;
	veiculos:boolean;
	consultoria:boolean;
	designTecnologia:boolean;
	eventos:boolean;
	modaBeleza:boolean;
	construcaoReforma:boolean;

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
		this.titleService.changeTitle('Categorias e serviços');
	  	this.breadcrumbService.changeRoute([], '');
	}

	ngOnInit() {
		this.assistenciaTecnica = true
		this.aulas = true;
		this.veiculos = true;
		this.consultoria = true;
		this.designTecnologia = true;
		this.eventos = true;
		this.modaBeleza = true;
		this.construcaoReforma = true;
		
	}

}
