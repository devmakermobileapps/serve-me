import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../header/title/title.service';

@Component({
  selector: 'app-push',
  templateUrl: './push.component.html',
  styleUrls: ['./push.component.scss']
})
export class PushComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
		this.titleService.changeTitle('Envio de Push');
	  	this.breadcrumbService.changeRoute([], '');
	}

  ngOnInit() {
  }

}
