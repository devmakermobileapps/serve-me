import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoPushComponent } from './novo-push.component';

describe('NovoPushComponent', () => {
  let component: NovoPushComponent;
  let fixture: ComponentFixture<NovoPushComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoPushComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoPushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
