import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../../header/title/title.service';

@Component({
  selector: 'app-novo-desconto',
  templateUrl: './novo-desconto.component.html',
  styleUrls: ['./novo-desconto.component.scss']
})
export class NovoDescontoComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
		this.titleService.changeTitle('Descontos');
	  	this.breadcrumbService.changeRoute([], '');
	}

	ngOnInit() {
	}


}
