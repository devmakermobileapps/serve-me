import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoDescontoComponent } from './novo-desconto.component';

describe('NovoDescontoComponent', () => {
  let component: NovoDescontoComponent;
  let fixture: ComponentFixture<NovoDescontoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoDescontoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoDescontoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
