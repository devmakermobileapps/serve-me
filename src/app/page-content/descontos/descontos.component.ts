import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../header/title/title.service';

@Component({
  selector: 'app-descontos',
  templateUrl: './descontos.component.html',
  styleUrls: ['./descontos.component.scss']
})
export class DescontosComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
		this.titleService.changeTitle('Descontos');
	  	this.breadcrumbService.changeRoute([], '');
	}

	ngOnInit() {
	}

}
