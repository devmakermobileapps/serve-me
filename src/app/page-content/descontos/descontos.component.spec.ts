import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescontosComponent } from './descontos.component';

describe('DescontosComponent', () => {
  let component: DescontosComponent;
  let fixture: ComponentFixture<DescontosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescontosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescontosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
