import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardInformacaoComponent } from './card-informacao.component';

describe('CardInformacaoComponent', () => {
  let component: CardInformacaoComponent;
  let fixture: ComponentFixture<CardInformacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardInformacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardInformacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
