import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-informacao',
  templateUrl: './card-informacao.component.html',
  styleUrls: ['./card-informacao.component.css']
})
export class CardInformacaoComponent implements OnInit {

	@Input() icon: string;
	@Input() tipoInformacao: string;
	@Input() informacao: string;

  constructor() { }

  ngOnInit() {
  }

}
