import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { BreadcrumbComponent } from '../../header/breadcrumb/breadcrumb.component';
import { TitleService } from '../../header/title/title.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	maisAcessados=[
		{ cidade:'Curitiba', estado:'Paraná', acessos: 401},
		{ cidade:'Curitiba', estado:'Paraná', acessos: 401},
		{ cidade:'Curitiba', estado:'Paraná', acessos: 401},
	]
	public barChartOptions:any = {
		scaleShowVerticalLines: false,
		responsive: true,
		elements: {
			line: {
				tension: 0, // disables bezier curves
			},
			point: {
				radius: 8,
				borderWidth: 3,
				hoverRadius: 8,
				hoverBorderWidth: 3
			}
		}
	  };
	  public barChartLabels:string[] = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun'];
	  public barChartType:string = 'line';
	  public barChartLegend:boolean = true;
	  public lineChartTension:number = 0;
	 
	  public barChartData:any[] = [
		{data: [170, 75, 110, 80, 100, 100], label: 'Orçamentos',},
		{data: [114, 169, 144, 121, 69, 102], label: 'Serviços'},
	  ];
	 
	  // events
	  public chartClicked(e:any):void {
		console.log(e);
	  }
	 
	  public chartHovered(e:any):void {
		console.log(e);
	  }

	  public lineChartColors:Array<any> = [
		{ // grey
			
		  backgroundColor: 'rgba(54,136,181,0.0)',
		  borderColor: 'rgba(54,136,181,1)',
		  pointBackgroundColor: 'rgba(255,255,255,0.6)',
		  pointBorderColor: 'rgba(54,136,181,1)',
		  pointHoverBackgroundColor: 'rgba(54,136,181,0.8)',
		  pointHoverBorderColor: 'rgba(255,255,255,1)'
		},
		{ // dark grey
		  backgroundColor: 'rgba(186,60,60,0.0)',
		  borderColor: 'rgba(186,60,60,1)',
		  pointBackgroundColor: 'rgba(255,255,255,0.6)',
		  pointBorderColor: 'rgba(186,60,60,1)',
		  pointHoverBackgroundColor: 'rgba(186,60,60,1)',
		  pointHoverBorderColor: 'rgba(255,255,255,1)'
		},
	  ];
	
	  public randomize():void {
		// Only Change 3 values
		let data = [
		  Math.round(Math.random() * 100),
		  59,
		  80,
		  (Math.random() * 100),
		  56,
		  (Math.random() * 100),
		  40];
		let clone = JSON.parse(JSON.stringify(this.barChartData));
		clone[0].data = data;
		this.barChartData = clone;
		/**
		 * (My guess), for Angular to recognize the change in the dataset
		 * it has to change the dataset variable directly,
		 * so one way around it, is to clone the data, change it and then
		 * assign it;
		 */
	  }
	  public barChartOptions2:any = {
	
		scales: {
				xAxes: [{
				stacked: true
				}],
			yAxes: [{
				stacked: true,
				barPercentage:0.5
			}]
		},
		elements: {
			rectangle:5
			// legend
		},
		  
		scaleShowVerticalLines: false,
		responsive: true
	  };
	  public barChartLabels2:string[] = ['Serviço 1','Serviço 2','Serviço 3','Serviço 4','Serviço 5',];
	  public barChartType2:string = 'horizontalBar';
	  public barChartLegend2:boolean = false;
	 
	  public barChartData2:any[] = [
		{data: [1900, 550, 560, 1300, 2200, 1600, 1700]},
	  ];
	 
	  // events
	  public chartClicked2(e:any):void {
		console.log(e);
	  }
	 
	  public chartHovered2(e:any):void {
		console.log(e);
	  }
	
	  public lineChartColors2:Array<any> = [
		{ // grey
			
		  backgroundColor: '#3688b5',
		  borderColor: '#3688b5',
		  pointBackgroundColor: 'rgba(0,120,172,1)',
		  pointBorderColor: '#fff',
		  pointHoverBackgroundColor: '#fff',
		  pointHoverBorderColor: 'rgba(0,120,172,0.8)'
		}
	  ];

  constructor(
	  private breadcrumbService:BreadcrumbService,
	  private titleService:TitleService
  ) { }

  ngOnInit() {
	  this.breadcrumbService.changeRoute([],'');
	  this.titleService.changeTitle('Bem-vindo');
  }

}
