import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../../../header/title/title.service';

@Component({
  selector: 'app-novo-usuario',
  templateUrl: './novo-usuario.component.html',
  styleUrls: ['./novo-usuario.component.css']
})
export class NovoUsuarioComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
	  this.breadcrumbService.changeRoute(['Gerenciar Usuários'], 'Novo usuário');
		this.titleService.changeTitle('Bem-vindo');
	}

	ngOnInit() {
	}

}
