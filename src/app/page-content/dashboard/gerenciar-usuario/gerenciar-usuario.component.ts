import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../../header/title/title.service';
import { BotoesHeaderService } from '../../../header/botoes-header/botoes-header.service';

@Component({
  selector: 'app-gerenciar-usuario',
  templateUrl: './gerenciar-usuario.component.html',
  styleUrls: ['./gerenciar-usuario.component.css']
})
export class GerenciarUsuarioComponent implements OnInit {

	constructor(
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService,
		private botoesHeaderService:BotoesHeaderService
		
	) { 
	  	this.breadcrumbService.changeRoute([], '');
		this.titleService.changeTitle('Bem-vindo');
		this.botoesHeaderService.toggleButton(true, 'dashboard/gerenciar-usuarios/novo')
	}

	ngOnInit() {
	}

}
