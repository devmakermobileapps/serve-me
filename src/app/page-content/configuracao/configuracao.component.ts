import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../header/title/title.service';

@Component({
  selector: 'app-configuracao',
  templateUrl: './configuracao.component.html',
  styleUrls: ['./configuracao.component.css']
})
export class ConfiguracaoComponent implements OnInit {

	headers = new Headers()
	produtos:any[] 
	taxForm:FormGroup;
	tipoProdForm:FormGroup;
	constructor(
		private breadcrumbService:BreadcrumbService,
		private fomrBuilder:FormBuilder,
		private http:Http,
		private titleService:TitleService
		
	) {
		this.titleService.changeTitle('Configurações');
		this.breadcrumbService.changeRoute([], '')
		this.headers.append('Content-Type', 'application/json');
		this.headers.append('Accept', 'application/json');
		this.headers.append('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vaG9tLWFncm9saXZlLmRldm1ha2VyZGlnaXRhbC5jb20uYnIvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE1MjA0NTM4NTQsIm5iZiI6MTUyMDQ1Mzg1NCwianRpIjoiRnJ3bW9NNEpYUnhrcWk2OCIsInN1YiI6MTQsInBydiI6ImY5MzA3ZWI1ZjI5YzcyYTkwZGJhYWVmMGUyNmYwMjYyZWRlODZmNTUifQ.pfswcnX4Wc9QKo-Z711gulf6CGnz8BaUKEghC4O9_LY')
	 }

  ngOnInit() {

	this.taxForm = this.fomrBuilder.group({
		taxaFixa:[null],
		taxaPercentual:[null]
	});
	this.tipoProdForm = this.fomrBuilder.group({
		nome:[null]
	})
	this.getProdutosTabela()

  }
  enviaTaxa(){
	//   console.log(this.taxForm.value)
  }

	getProdutosTabela(){
		

		return this.http.get(`http://hom-agrolive.devmakerdigital.com.br/api/categories`, {headers:this.headers})
		.toPromise()
		.then(res => {
			res.json().data
			this.produtos = res.json().data.categories
			// console.log(this.produtos)
		})
		
	}
	

}
