import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoAdminComponent } from './novo-admin.component';

describe('NovoAdminComponent', () => {
  let component: NovoAdminComponent;
  let fixture: ComponentFixture<NovoAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
