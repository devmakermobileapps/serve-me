import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalhesOrcamentosComponent } from './detalhes-orcamentos.component';

describe('DetalhesOrcamentosComponent', () => {
  let component: DetalhesOrcamentosComponent;
  let fixture: ComponentFixture<DetalhesOrcamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalhesOrcamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalhesOrcamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
