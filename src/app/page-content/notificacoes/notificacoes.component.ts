import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BreadcrumbService } from '../../header/breadcrumb/breadcrumb.service';
import { TitleService } from '../../header/title/title.service';


@Component({
  selector: 'app-notificacoes',
  templateUrl: './notificacoes.component.html',
  styleUrls: ['./notificacoes.component.css']
})
export class NotificacoesComponent implements OnInit {

	notificationForm:FormGroup;

	constructor(
		private formBuilder:FormBuilder,
		private breadcrumbService:BreadcrumbService,
		private titleService:TitleService
		
	) { 
	  	this.breadcrumbService.changeRoute([], '');
		this.titleService.changeTitle('Notificações');
	  
	}

	ngOnInit() {
		this.notificationForm =  this.formBuilder.group({
			addImg:[null],
			titulo:[null],
			resumo:[null],
			link:[null]
		})
	}

}
