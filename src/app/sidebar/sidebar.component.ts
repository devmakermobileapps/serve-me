import { Component, OnInit, EventEmitter } from '@angular/core';
import { PerfectScrollbarConfig, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

	toggleMenu: boolean = false;
	public config: PerfectScrollbarConfigInterface = {
		wheelSpeed:0.5
	};
  	constructor() { }

	ngOnInit() {
	}
	toggleSubmenu(){
		this.toggleMenu = !this.toggleMenu;
	}
}

