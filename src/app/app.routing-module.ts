import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes }  from '@angular/router';



// import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './page-content/dashboard/dashboard.component';
import { ConfiguracaoComponent } from './page-content/configuracao/configuracao.component';
import { NotificacoesComponent } from './page-content/notificacoes/notificacoes.component';
import { GerenciarUsuarioComponent } from './page-content/dashboard/gerenciar-usuario/gerenciar-usuario.component';
import { NovoUsuarioComponent } from './page-content/dashboard/gerenciar-usuario/novo-usuario/novo-usuario.component';
import { OrcamentosComponent } from './page-content/orcamentos/orcamentos.component'
import { ServicosComponent } from './page-content/servicos/servicos.component';
import { CategoriasServicosComponent } from './page-content/categorias-servicos/categorias-servicos.component';
import { PrestadoresComponent } from './page-content/usuarios/prestadores/prestadores.component';
import { ClientesComponent } from './page-content/usuarios/clientes/clientes.component';
import { DescontosComponent } from './page-content/descontos/descontos.component';
import { PagamentosComponent } from './page-content/pagamentos/pagamentos.component';
import { RelatoriosComponent } from './page-content/relatorios/relatorios.component';
import { PushComponent } from './page-content/push/push.component';
import { FaleConoscoComponent } from './page-content/fale-conosco/fale-conosco.component';
import { AdminsComponent } from './page-content/admins/admins.component';
import { NovoAdminComponent } from './page-content/admins/novo-admin/novo-admin.component';
import { NovoPushComponent } from './page-content/push/novo-push/novo-push.component';
import { NovoServicoComponent } from './page-content/categorias-servicos/servico/novo-servico/novo-servico.component';
import { DetalhesClienteComponent } from './page-content/usuarios/clientes/detalhes-cliente/detalhes-cliente.component';
import { DetalhesPrestadorComponent } from './page-content/usuarios/prestadores/detalhes-prestador/detalhes-prestador.component';
import { DetalhesOrcamentosComponent } from './page-content/orcamentos/detalhes-orcamentos/detalhes-orcamentos.component';
import { LoginComponent } from './login/login.component';
import { ServicoComponent } from './page-content/categorias-servicos/servico/servico.component';





const appRoutes: Routes = [
	{ path: 'login',												component: LoginComponent },
	{ path: 'dashboard',											component: DashboardComponent },
	{ path: 'dashboard/gerenciar-usuarios',							component: GerenciarUsuarioComponent },
	{ path: 'dashboard/gerenciar-usuarios/novo',					component: NovoUsuarioComponent },
	{ path: 'configuracao',											component: ConfiguracaoComponent },
	{ path: 'descontos',											component: DescontosComponent },
	{ path: 'envio-de-push',										component: PushComponent },
	{ path: 'envio-de-push/novo',									component: NovoPushComponent },
	{ path: 'usuarios/clientes',									component: ClientesComponent},
	{ path: 'usuarios/clientes/detalhes',							component: DetalhesClienteComponent},
	{ path: 'usuarios/prestadores',									component: PrestadoresComponent},
	{ path: 'usuarios/prestadores/detalhes',						component: DetalhesPrestadorComponent},
	{ path: 'orcamentos',											component: OrcamentosComponent},
	{ path: 'orcamentos/detalhes',									component: DetalhesOrcamentosComponent},
	{ path: 'servicos',												component: ServicosComponent},
	{ path: 'categorias-e-servicos',								component: CategoriasServicosComponent},
	{ path: 'categorias-e-servicos/servicos',						component: ServicoComponent},
	{ path: 'categorias-e-servicos/servicos/novo',					component: NovoServicoComponent},
	{ path: 'pagamentos',											component: PagamentosComponent},
	{ path: 'relatorios',											component: RelatoriosComponent},
	{ path: 'fale-conosco',											component: FaleConoscoComponent},
	{ path: 'admins',												component: AdminsComponent},
	{ path: 'admins/novo',											component: NovoAdminComponent},
	{ path: '',   redirectTo: '/dashboard', pathMatch: 'full' },
	{ path: '**', component:  DashboardComponent}
]

@NgModule({
	imports: [
	  CommonModule,
	  RouterModule.forRoot(
		  appRoutes,
	  )
	],
	exports:[
	  RouterModule
	],
	declarations: []
  })
export class AppRoutingModule { }