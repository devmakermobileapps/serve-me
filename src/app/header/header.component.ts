import { Component, OnInit } from '@angular/core';
import	{Router, NavigationEnd, ActivatedRoute}								from '@angular/router';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	tituloPagina:string;
	breadcrumb
	_route:any;
	closeResult: string;

  constructor(
	private router: Router, 
	private modalService: NgbModal
  ) { 
	router.events.subscribe((val) => {
		// this.addCidade = false;
		// this.addParametro = false;
		// this.addNotificacao = false;
		// this.addUsuario = false;
		this._route = router
		// console.log(this.router.url)
		if(router.url == '/dashboard'){
			this.tituloPagina = 'Dashboard'
			// this.addCidade = false;
			// this.addParametro = false;
			// this.addNotificacao = false;
			// this.addUsuario = false;
		}
		if(router.url == '/meeting-points'){
			this.tituloPagina = 'Meeting Points'
			// this.addCidade = false;
			// this.addParametro = false;
			// this.addNotificacao = false;
			// this.addUsuario = false;
		}
		if(router.url == '/notificacoes'){
			this.tituloPagina = 'Notificações'
			// this.addCidade = false;
			// this.addParametro = false;
			// this.addNotificacao = true;
			// this.addUsuario = false;
		}
		if(router.url == '/configuracoes'){
			this.tituloPagina = 'Configurações'
			// this.addCidade = true;
			// this.addParametro = false;
			// this.addNotificacao = false;
			// this.addUsuario = false;
		}
		if(router.url != undefined){

			this.breadcrumb =  router.url.toString()
			this.breadcrumb = this.breadcrumb.split(/\//);
			// console.log('com o primeiro',this.breadcrumb)
			// this.breadcrumb.forEach(function(item, index, object) {
			// 	if (item === '') {
			// 	  object.splice(index, 1);
			// 	}
			// });
		}
		// console.log(router.url) 
	});
  }

  ngOnInit() {
  }

  resizeTheFuckingDatatable(){

	setTimeout(() => {
		window.dispatchEvent(new Event('resize'));
	}, 320);
	
}

	addBtn(){
		
	}
	open(content) {
		this.modalService.open(content, { windowClass: 'modal-width' }).result.then((result) => {
		  this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
		  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	  }
	
	  private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
		  return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
		  return 'by clicking on a backdrop';
		} else {
		  return  `with: ${reason}`;
		}
	  }
}
