import { TestBed, inject } from '@angular/core/testing';

import { BotoesHeaderService } from './botoes-header.service';

describe('BotoesHeaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BotoesHeaderService]
    });
  });

  it('should be created', inject([BotoesHeaderService], (service: BotoesHeaderService) => {
    expect(service).toBeTruthy();
  }));
});
