import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotoesHeaderComponent } from './botoes-header.component';

describe('BotoesHeaderComponent', () => {
  let component: BotoesHeaderComponent;
  let fixture: ComponentFixture<BotoesHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotoesHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotoesHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
