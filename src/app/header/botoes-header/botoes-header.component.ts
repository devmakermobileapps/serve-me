import { Component, OnInit } from '@angular/core';
import { BotoesHeaderService } from './botoes-header.service';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-botoes-header',
  templateUrl: './botoes-header.component.html',
  styleUrls: ['./botoes-header.component.css']
})
export class BotoesHeaderComponent implements OnInit {

	buttonAtivo:boolean = false;
	rota:string = '';

	constructor(
		private router:Router
	) { 
		router.events.subscribe(
			(val) =>{
				// console.log(val instanceof NavigationStart)
				if(val instanceof NavigationStart == true){
					this.buttonAtivo = false
				}
			}
		)
	}

	ngOnInit() {
		BotoesHeaderService.rotaEmmiter.subscribe(
			event => this.rota = event
		)
		BotoesHeaderService.estadoEmmiter.subscribe(
			event => this.buttonAtivo = event
		)
	}
	addBtn(){
		
	}

}
