import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class BotoesHeaderService {

	estado:boolean;
	static estadoEmmiter = new EventEmitter<boolean>()
	rota:string;
	static rotaEmmiter = new EventEmitter<string>()

  	constructor() { }

		toggleButton(estado:boolean, rota:string){
			this.estado = estado;
			BotoesHeaderService.estadoEmmiter.emit(estado);

			this.rota = rota;
			BotoesHeaderService.rotaEmmiter.emit(rota);
		}


}
