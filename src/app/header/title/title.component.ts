import { Component, OnInit } from '@angular/core';
import { TitleService } from './title.service';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

	title: string;
	constructor() { }

	ngOnInit() {
		TitleService.titleEmitter.subscribe(
			title => this.title = title
		)
	}

}
