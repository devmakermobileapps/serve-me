import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class TitleService {

	title: string;
	static titleEmitter = new EventEmitter<string>()

	constructor() { }

	changeTitle(title:string){
		this.title = title
		TitleService.titleEmitter.emit(title)
	}

}
