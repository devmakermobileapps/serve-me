import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BreadcrumbService } from './breadcrumb.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
	
	current: string = '';
	previous: string[] = [];
	
	constructor() { 
		
	}
	
	ngOnInit() {
		BreadcrumbService.currentEmitrer.subscribe(
			current => this.current = (current)
		)
		BreadcrumbService.previousEmitrer.subscribe(
			previous => this.previous = (previous)
		)
	}


}
