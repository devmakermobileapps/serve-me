import { Injectable,EventEmitter } from '@angular/core';

@Injectable()
export class BreadcrumbService {

	static currentEmitrer = new EventEmitter<string>()

	static previousEmitrer = new EventEmitter<string[]>()

	current:string;

	previous:string[];

	constructor() { }

	changeRoute(newRoute:string[], newCurrentRoute:string){
		this.previous = newRoute;
		BreadcrumbService.previousEmitrer.emit(newRoute);
		this.current = newCurrentRoute;
		BreadcrumbService.currentEmitrer.emit(newCurrentRoute)
	}

}
