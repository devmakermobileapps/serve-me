import { TestBed, inject } from '@angular/core/testing';

import { UserCredentialService } from './user-credential.service';

describe('UserCredentialService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserCredentialService]
    });
  });

  it('should be created', inject([UserCredentialService], (service: UserCredentialService) => {
    expect(service).toBeTruthy();
  }));
});
