import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class UserCredentialService {

	user:boolean;
	static userEmitter = new EventEmitter<boolean>();


  constructor() { }

	toggleUser(user:boolean){
		this.user = user
		UserCredentialService.userEmitter.emit(user)
	}
}
