import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { UserCredentialService } from './user-credential.service';

@Component({
  selector: 'app-user-credentials',
  templateUrl: './user-credentials.component.html',
  styleUrls: ['./user-credentials.component.scss']
})
export class UserCredentialsComponent implements OnInit {

	user:boolean;

	constructor(
		private router:Router
	) { 
		router.events.subscribe(
			(val) =>{
				// console.log(val instanceof NavigationStart)
				if(val instanceof NavigationStart == true){
					this.user = false
				}
			}
		)
	}

  ngOnInit() {
	  UserCredentialService.userEmitter.subscribe(
		  event => this.user = event
	  )
  }

}
